1
00:00:00,080 --> 00:00:04,799
In this part you are going to analyze

2
00:00:02,639 --> 00:00:07,520
the kernel pools with your debugger

3
00:00:04,799 --> 00:00:10,320
WinDbg. The goal is going to be to set a

4
00:00:07,520 --> 00:00:12,880
breakpoint on the ExAllocatePoolWithTag

5
00:00:10,320 --> 00:00:14,799
function and then continue execution

6
00:00:12,880 --> 00:00:16,800
to make sure the allocation happens and

7
00:00:14,799 --> 00:00:19,840
then you'll be able to analyze the back

8
00:00:16,800 --> 00:00:22,960
trace the actual object being allocated

9
00:00:19,840 --> 00:00:26,880
and its size, as well as the pool type.

10
00:00:22,960 --> 00:00:26,880
Okay, now it's your turn.

